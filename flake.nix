{
  inputs = {
    m100-utils-flake.url = "gitlab:trs-80-model-100/utils";
    asxxxx-flake.url = "gitlab:trs-80-model-100/asxxxx-nix-flake";
  };
  outputs = { self, nixpkgs, flake-utils, m100-utils-flake, asxxxx-flake }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        asxxxx = asxxxx-flake.packages.${system}.default;

        m100-utils = m100-utils-flake.packages.${system}.default;
      in
      {
        packages.default = asxxxx;

        devShells.default = with pkgs; mkShell {
          packages = [
            asxxxx
            m100-utils
            graphviz
          ];

          buildInputs = [
            gnumake
          ];

          M100_UTILS_MK = "${m100-utils}/make";
        };
      }
    );
}
