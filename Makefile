include $(M100_UTILS_MK)/asm.mk

all: zmodem.do header.svg

%.svg: %.dot
	dot -Tsvg < $< > $@

ChangeLog:
	build-aux/gitlog-to-changelog > ChangeLog

.PHONY: clean
clean:
	rm -f zmodem.do
