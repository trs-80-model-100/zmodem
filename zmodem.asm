; zmodem.asm -- ZMODEM sender and receiver for the TRS-80 Model 100
;
; Copyright (C) 2024  Cyrill Schenkel
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

.8085

.area CODE (ABS)
.ifdef CO_HEAD
.org 0xEA60 - 6 ; .CO header is 6 bytes long
.else
.org 0xEA60
.endif
.end _start

.define VERSION "0.1"

; ROM procedures
.define $chget     "0x12DB"
.define $setser    "0x17E6" ; set COM mode
.define $menu      "0x5797"
.define $rv232c    "0x6D7E"
.define $sd232c    "0x6E32"
.define $clscom    "0x6ECB"
.define $rcvx      "0x6D6D"
.define $kyread    "0x7242"

.define $_puts     "0x11A2" ; print zero terminated string (ptr in HL)
.define $_files    "0x1F3A" ; BASIC FILES procedure

; ZMODEM constants
.define ZPAD       "0x2A"
.define ZDLE       "0x18"
.define ZBIN       "0x41"
.define ZHEX       "0x42"

.ifdef CO_HEAD
.word 0xEA60         ; origin
.word $bottom - $top ; size
.word _start         ; transfer
.endif

$top:

_start:
        ; clear screen
        mvi a, 0x0C
        rst 4

        ; print copyright notice
        lxi h, $cr_notice
        call $_puts

        ; setup UART
        stc
        lxi h, $mode
        call $setser

event_loop:
        call $rcvx
        jz _event_loop_skip_recv
        call $rv232c
        jc _event_loop_skip_recv
        call dispatch
_event_loop_skip_recv:
        call $kyread
        jz event_loop
        mvi b, 0x71
        cmp b
        jz _event_loop_exit
        jmp event_loop
_event_loop_exit:

        ; clean up
        call $clscom

        ; end program
        jmp $menu

; PROC: Handle incoming RS232 communications.
; pre : received word in A
; post: -
dispatch:
        lhld $dispatch_next
        pchl

dispatch_reset:
        lxi h, hdr_p0
        shld $dispatch_next
        ret

; END dispatch

; BEGIN Packet Header Parser
;
; See `header.dot' for details.

hdr_p0:
        mvi b, ZPAD
        cmp b
        rnz
        lxi h, hdr_p1
        shld $dispatch_next
        ret

hdr_p1:
        mvi b, ZDLE
        cmp b
        jnz _hdr_p1_ZPAD
        lxi h, hdr_p2
        shld $dispatch_next
        ret
_hdr_p1_ZPAD:
        mvi b, ZPAD
        cmp b
        jnz dispatch_reset
        lxi h, hdr_p3
        shld $dispatch_next
        ret

hdr_p2:
        mvi b, ZBIN
        cmp b
        jnz dispatch_reset
        lxi h, hdr_bin1
        shld $dispatch_next
        ret

hdr_p3:
        mvi b, ZDLE
        cmp b
        jnz dispatch_reset
        lxi h, hdr_p4
        shld $dispatch_next
        ret

hdr_p4:
        mvi b, ZHEX
        cmp b
        jnz dispatch_reset
        lxi h, hdr_hex
        shld $dispatch_next
        ret

hdr_bin1: ; TODO implement binary header parsing
        call to_hex
        rst 4
        mov a, b
        rst 4
        mvi a, 0x09
        rst 4
        jmp dispatch_reset

hdr_hex:
        lxi h, $header
        shld $header_cursor

hdr_hex_high:
        call from_hex
        jc dispatch_reset
        mvi e, 4
_hdr_hex_high_rot:
        ral
        dcr e
        jnz _hdr_hex_high_rot
        lhld $header_cursor
        xchg
        stax d
        lxi h, hdr_hex_low
        shld $dispatch_next
        ret

hdr_hex_low:
        call from_hex
        jc dispatch_reset
        lhld $header_cursor
        add m
        xchg
        stax d
        lxi h, $header_end
        rst 3 ; de == hl
        jz hdr_hex_end
        lxi h, $header_cursor
        inr m
        lxi h, hdr_hex_high
        shld $dispatch_next
        ret

hdr_hex_end:
        call handle_packet
        ret

; END Packet Header Parser

; PROC: Handle last received packet header.
; pre : valid packet header in $header
; post: C is set to 1 if unsucessful
handle_packet:
        ret
; END handle_packet

; PROC: Convert A from ASCII hex.
; pre : input in A
; post: output in A if input is a valid hex digit
;       otherwise C is set to 1
from_hex:
        mvi b, 0x30
        sub b
        jm _from_hex_invalid
        mvi b, 0xA
        cmp b
        jp _from_hex_high
        stc
        cmc
        ret

_from_hex_high:
        mvi b, 0x07
        sub b
        jm _from_hex_invalid
        mvi b, 0x10
        cmp b
        jm _from_hex_end

_from_hex_invalid:
.ifndef RELEASE
        mvi a, 0x21
        rst 4
.endif
        stc
        ret

_from_hex_end:
        stc
        cmc
        ret
; END from_hex

; PROC: Convert A to ASCII hex.
; pre : input in A
; post: high nibble in A, low nibble in B
to_hex:
        mov c, a ; backup in c
        mvi b, 0x0F
        ana b
        lxi h, $hex
        mvi d, 0
        mov e, a
        dad d
        xchg
        ldax d
        mov b, a
        ; most significant nibble next
        mov a, c
        mvi e, 4
_to_hex_rot:
        rar
        dcr e
        jnz _to_hex_rot
        mvi e, 0x0F
        ana e
        lxi h, $hex
        mvi d, 0
        mov e, a
        dad d
        xchg
        ldax d
        ret
; END to_hex

$hex:   .ascii "0123456789ABCDEF"

$cr_notice:
        .asciz "ZMODEM               (C) 2024  Cyrill S."

$mode:  .ascii "38N1E,10"

$dispatch_next:
        .dw hdr_p0

$header_cursor:
        .dw $header

$header:
        .blkb 7 ; type + 4 * data + 2 * crc
        $header_end = . - 1

$bottom:

.ifdef CO_HEAD
.byte 0x00 ; EOF marker
.endif
